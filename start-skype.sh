#!/bin/bash

container_id=$(docker ps -a | grep skype | awk -F" " '{print $1}' | tr "\n" " ")
[ ! -z "${container_id}" ] && docker rm --force ${container_id} 2>&1 > /dev/null

# Remove old fingerprints of the docker container
ssh-keygen -R [127.0.0.1]:55555

# Absolute path to the script's directory
my_path=$(dirname `realpath $0`)

# Create these directories if they don't exist already
mkdir -p ${my_path}/.Skype
mkdir -p ${my_path}/downloads

# Start a container with skype based on the 'skype' image
docker run \
    --detach \
    --publish=55555:22 \
    --name skype \
    --volume=${my_path}/.Skype:/home/docker/.Skype \
    --volume=${my_path}/downloads:/home/docker/Downloads \
    --device=/dev/video0:/dev/video0:rwm \
    skype

sleep 0.25s

# Start skype through ssh
ssh -f -oStrictHostKeyChecking=no docker-skype skype-pulseaudio

sleep 1s

while [ ! -z "$(pidof skype)" ]
do
    sleep 0.5s
done

container_id=$(docker ps -a | grep skype | awk -F" " '{print $1}' | tr "\n" " ")
[ ! -z "${container_id}" ] && docker rm --force ${container_id} 2>&1 > /dev/null
