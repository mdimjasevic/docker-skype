FROM jessie:i386
MAINTAINER Marko Dimjašević <marko@dimjasevic.net>

# Tell debconf to run in non-interactive mode
ENV DEBIAN_FRONTEND noninteractive

# Make sure the repository information is up to date
RUN apt-get update

# Install dependencies and immediately clean-up
ENV PACKAGES libpulse0 pulseaudio openssh-server wget
RUN apt-get install -y $PACKAGES && apt-get clean


# Install Skype
ENV SKYPE_PATH /tmp/skype.deb
RUN wget --output-document $SKYPE_PATH \
    	 http://download.skype.com/linux/skype-debian_4.3.0.37-1_i386.deb && \
    (dpkg --install $SKYPE_PATH || true) && \
    rm $SKYPE_PATH

# Automatically detect and install dependencies
RUN apt-get install -fy && apt-get clean

# Create user "docker", set the password to "docker", and add it to
# the "video" group
RUN useradd -m -d /home/docker docker
RUN echo "docker:docker" | chpasswd
RUN usermod -aG video docker

# Create OpenSSH privilege separation directory, enable X11Forwarding
RUN mkdir -p /var/run/sshd
RUN echo X11Forwarding yes >> /etc/ssh/ssh_config

# Prepare ssh config folder so we can upload SSH public key later
RUN mkdir /home/docker/.ssh
RUN chown -R docker:docker /home/docker
RUN chown -R docker:docker /home/docker/.ssh

ADD id_rsa.pub /home/docker/.ssh/authorized_keys

# Set locale (fix locale warnings)
RUN localedef -v -c -i en_US -f UTF-8 en_US.UTF-8 || true
RUN echo "America/Denver" > /etc/timezone

# Set up the launch wrapper - sets up PulseAudio to work correctly
ADD skype-pulseaudio /usr/local/bin/skype-pulseaudio
RUN chmod 755 /usr/local/bin/skype-pulseaudio


# Expose the SSH port
EXPOSE 22

# Start SSH
ENTRYPOINT ["/usr/sbin/sshd",  "-D"]
