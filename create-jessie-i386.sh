#!/bin/bash

jessie_dir=jessie-i386
sudo debootstrap --arch=i386 --variant=minbase jessie ${jessie_dir} http://httpredir.debian.org/debian
sudo tar --directory ${jessie_dir} -c . | docker import - jessie:i386
